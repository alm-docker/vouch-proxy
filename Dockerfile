# voucher/vouch-proxy
# https://github.com/vouch/vouch-proxy
FROM golang:1.13.7-alpine AS builder

ARG VOUCH_VERSION
RUN update-ca-certificates && \
    mkdir dummy && cd dummy && go mod init dummy && \
    go get -d -v github.com/vouch/vouch-proxy@v$VOUCH_VERSION && \
    GOARCH=_GOCROSS go build -o vouch-proxy github.com/vouch/vouch-proxy

FROM scratch
ARG VOUCH_VERSION
LABEL maintainer="vouch@bnf.net"
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/ca-certificates.crt
COPY --from=builder /go/pkg/mod/github.com/vouch/vouch-proxy\@v$VOUCH_VERSION/templates/ templates/
COPY --from=builder /go/pkg/mod/github.com/vouch/vouch-proxy\@v$VOUCH_VERSION/static/ static/
COPY --from=builder /go/dummy/vouch-proxy /vouch-proxy
EXPOSE 9090
ENTRYPOINT ["/vouch-proxy"]
HEALTHCHECK --interval=1m --timeout=5s CMD [ "/vouch-proxy", "-healthcheck" ]
